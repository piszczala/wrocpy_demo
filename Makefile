setup_flask:
	cd flask_demo && make build

setup_django:
	cd django_demo && make build

setup: setup_flask setup_django

requirements:
	sudo apt-get install docker docker-compose

full_setup: requirements setup