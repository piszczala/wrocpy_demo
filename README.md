# WrocPy demo

## Commands

    make full_setup

## Development

- Flask: <http://0.0.0.0:5000/>
- Django: <http://0.0.0.0:8000/>

## Deployment

- Flask: <https://wrocpy-demo.herokuapp.com/>
- Django: <http://ec2-13-53-46-83.eu-north-1.compute.amazonaws.com/>
